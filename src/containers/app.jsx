'use strict';
import _ from 'lodash'
import React from 'react'
import DocumentTitle from 'react-document-title'
import { connect } from 'react-redux'

import Header from '../components/header.jsx'

class App extends React.Component {
    
    getSuggestions() {
        const suggestions = [];
    
        if (this.props.data.tags) {
            suggestions.push({
                            title: 'tag:',
                            description: 'Filter resources based on tags',
                            query:'tag:'
                        });
        }   
        
        if (this.props.data.securityDefinitions && _.find(this.props.data.securityDefinitions, { 'type': 'oauth2'})) {
            suggestions.push({
                            title: 'scope:',
                            description: 'Filter resources based on the required OAuth2 scope',
                            query:'scope:'
                        });
        }    
        
        return suggestions;
    }

    render() {
        const { data, nav, location } = this.props;
        return (
            <DocumentTitle title={data.info.title}>
                <section id="content" role="main">
                    <Header title={data.info.title}
                            suggestions={this.getSuggestions()}
                            location={location}/>
                    {this.props.children}
                </section>
            </DocumentTitle>);       
    }
}

export default connect(state => state, {})(App);
