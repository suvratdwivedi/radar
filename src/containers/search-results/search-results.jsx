'use strict';
import './search-results.less'

import _ from 'lodash'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { filterPaths, filterPathsByTag, filterPathsByScope } from '../../reducers/data-reducer'
import { setBasePath } from '../../utils/basepath-util'

import Page from '../../components/aui/page/page.jsx'
import PathList from '../../components/path-list/path-list.jsx'
import TagList from '../../components/tag-list/tag-list.jsx'
import ScopeList from '../../components/scope-list/scope-list.jsx'

class SearchResults extends Component {
    
    content() {
        const { nav, data } = this.props;
        
        const { q } = this.props.location.query;
        let results;
        if (q.indexOf('tag:') === 0) {
            if (q === 'tag:') {
                return <TagList tags={data.tags}/>
            } else {
                results = filterPathsByTag(data.paths, q.replace('tag:', ''));
            }
        } else if (q.indexOf('scope:') === 0) {
            if (q === 'scope:') {
                return <ScopeList scopes={_.find(this.props.data.securityDefinitions, { 'type': 'oauth2'}, {}).scopes}/>
            } else {
                results = filterPathsByScope(data.paths, q.replace('scope:', ''));
            }          
        } else {
            results = filterPaths(data.paths, q);
        }

        if (Object.keys(results).length === 0) {            
            return <div className="empty-search">
                    <img src={linkWithBase("/images/hunter.png")} />
                    <h3>We couldn't find any results matching '{q}'.</h3>
                    <p>Hunter, our highly trained search dog, seems to have lost the scent.</p>
                </div>
        } else {
            return <PathList resources={results}/>
        }        
    }
    
    render() {
        return (<Page>{this.content()}</Page>);
    }
}

export default connect(state => state, {})(SearchResults);