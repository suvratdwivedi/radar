'use strict';
import './meta-page.less'

import React from 'react'
import { connect } from 'react-redux'

import Page from '../../components/aui/page/page.jsx'
import MetaContent from '../../components/meta-content/meta-content.jsx'

class MetaPage extends React.Component {

    render() {
        const { data, params } = this.props;
        for (const page of data['x-radar-pages']) {
            if (page.key == params.name) {
                return (<Page>
                    <MetaContent page={page}/>
                </Page>);
            }
        }
    }
}

export default connect(s => s, {})(MetaPage);