import { createStore, applyMiddleware, compose } from 'redux'

export default function configureStore(reducers) {
    return createStore(reducers);
}