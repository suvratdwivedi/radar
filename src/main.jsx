'use strict';
import React from 'react'
import { render } from 'react-dom'
import { IndexRoute, Router, Route, useRouterHistory } from 'react-router'
import { Provider } from 'react-redux'
import { routerReducer } from 'react-router-redux'
import { combineReducers } from 'redux'
import RefParser from 'json-schema-ref-parser'

import Root from './containers/root/root'
import Overview from './containers/api-overview/api-overview.jsx'
import MetaPage from './containers/meta-page/meta-page.jsx'
import RestResource from './containers/rest-resource.jsx'
import SearchResults from './containers/search-results/search-results.jsx'
import SecurityOverview from './containers/security-overview/security-overview.jsx'
import * as reducers from './reducers/all-reducers'
import { dataLoad } from './action-creators/data-action-creators'
import configureStore from './store/configure-store'

import { initHistory, getHistory } from './utils/route-util'
import { addDefinitionTitles } from './utils/swagger-util'
import { setBasePath } from './utils/basepath-util'

const store = configureStore(combineReducers({
    ...reducers,
    routing: routerReducer
}));
initHistory(store, window.ATLASSIAN.context);
setBasePath(window.ATLASSIAN.basePath);

window.addEventListener('load', function() {    
    RefParser.dereference(addDefinitionTitles(window.ATLASSIAN.data))
        .then((data) => {
            store.dispatch(dataLoad(data));
            render(<Provider store={store}>
                    <Router history={getHistory()}>
                        <Route path="/" component={Root}>
                            <Route path="resource/*" component={RestResource}/> 
                            <Route path="search" component={SearchResults}/> 
                            <Route path="security" component={SecurityOverview}/> 
                            <Route path="meta/:name" component={MetaPage}/>
                            <IndexRoute component={Overview}/>
                        </Route>
                    </Router>
                </Provider>, document.getElementById('app'));
        });    
}, false);
