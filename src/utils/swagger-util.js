'use strict';

/**
 * Ensures the base path does not end in a slash.
 */
export function cleanBasePath(value) {
    value = value || '';
    return value.slice(-1) === '/' ? value.substring(0, value.length - 1) : value;
}

export function addDefinitionTitles(data) {
    if (data.definitions) {
        Object.keys(data.definitions).forEach(key => !data.definitions[key].title ? data.definitions[key].title = key : null);
    }
    
    return data;
}