'use strict';

export function hash() {
    return window.location.hash.substring(1);
}

export function query(value) {  
    const queryString = window.location.search.substring(1); // discard the leading '?'
    
    let queryArray = queryString.split('&');
    let queryMap = {};
    
    queryArray.forEach(val => {
        let arr = val.split('=');
        queryMap[arr[0]] = decodeURIComponent(arr[1]);
    });
    
    return queryMap[value];    
}