'use strict';
import './scope-list.less'

import _ from 'lodash'
import React from 'react'
import { Link } from 'react-router'
import Markdown from 'react-markdown'

export default class ScopeList extends React.Component {
        
	renderScopes() {
        const { scopes } = this.props;
        if (!scopes) {
            return null;
        }
		const description = (value) => value ? <Markdown source={value} skipHtml={true}/> : <span className="no-description">(No description)</span>;
        
		return Object.keys(scopes).map((name) => <tr key={name}>
				<td key="scope-name"><Link to={'/search?q=scope:' + name}>{name}</Link></td>
				<td key="scope-description">{description(scopes[name])}</td>
			</tr>);
	}

	render() {
		return <div>
                <h3>Scopes</h3>
                <table className='aui resource-list-table'>
				    <tbody>
					   {this.renderScopes()}
				    </tbody>
			     </table>
            </div>
	}
}
