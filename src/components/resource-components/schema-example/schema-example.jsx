'use strict';
import React from 'react'

import Example from '../example.jsx'
import JsonSchema from '../json-schema/json-schema.jsx'
import Tabs from '../../aui/tabs/tabs.jsx'

export default class SchemaExample extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            selectedTab: null
        }; 
    }

    render() {
        const { key, schema, example } = this.props;
        const setTab = (selectedTab) => { this.setState({selectedTab}) };

        if (schema && example) {
            const tabValues = [{
                id: 'schema',
                label: 'Model',
                content: <JsonSchema key={key + '-schema'} data={schema} />
            }, {
                id: 'example',
                label: 'Example',
                content: <Example key={key + '-example'} value={example}/>
            }];
            return <Tabs values={tabValues}
                            layout={Tabs.options().layout.HORIZONTAL}
                            selectedTab={this.state.selectedTab}
                            noHash={true}
                            onClick={setTab}/>
        } else if (schema) {
            return <JsonSchema key={key + '-schema'} data={schema}/>
        } else if (example) {
            return <Example key={key + '-example'} value={example}/>            
        }

        return null;
    }
}