jest.disableAutomock();

import React from 'react'
import { shallow } from 'enzyme'

import Example from '../../example.jsx'
import JsonSchema from '../../json-schema/json-schema.jsx'
import Tabs from '../../../aui/tabs/tabs.jsx'

import SchemaExample from '../schema-example.jsx'

describe('schema-example', () => {
    const example = {
        "something": "exciting"
    };
    
    const schema = {
        "type": "string"
    };

    it('renders only the example if there is no schema', () => {
        const wrapper = shallow(<SchemaExample key='test' example={example} />);
        expect(wrapper.find(Example).length).toEqual(1);
        expect(wrapper.find(JsonSchema).length).toEqual(0);
        expect(wrapper.find(Tabs).length).toEqual(0);
    })

    it('renders only the schema if there is no example', () => {
        const wrapper = shallow(<SchemaExample key='test' schema={schema} />);
        expect(wrapper.find(Example).length).toEqual(0);
        expect(wrapper.find(JsonSchema).length).toEqual(1);
        expect(wrapper.find(Tabs).length).toEqual(0);
    })

    it('renders the example and schema', () => {
        const wrapper = shallow(<SchemaExample key='test' schema={schema} example={example} />);
        expect(wrapper.find(Example).length).toEqual(0);
        expect(wrapper.find(JsonSchema).length).toEqual(0);
        expect(wrapper.find(Tabs).length).toEqual(1);

        const tabs = wrapper.find(Tabs).shallow();
        expect(tabs.children().find(Example).length).toEqual(1);
        expect(tabs.children().find(JsonSchema).length).toEqual(1);
    })
})