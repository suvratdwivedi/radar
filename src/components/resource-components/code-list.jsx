'use strict'; 
import React from 'react'

export default class CodeList extends React.Component {
    
    render() {
        const { title, values } = this.props;
        if (values) {
            return (<div className="resource-section">
                        <h4>{title}</h4>
                        <ul>
                            {values.map(value => <li key={value}>
                                <pre>{value}</pre>
                            </li>)}
                        </ul>
                    </div>);
        } else {        
            return null;
        }
    }
}