'use strict'; 
import React from 'react'
import Highlight from 'react-highlight'

export default class Example extends React.Component {
    
    static formatJson(value) {
        try {
            value = JSON.parse(value);
        } catch (e) {
        }
        try {
            return JSON.stringify(value, null, 4);
        } catch (e) {
        }
        return String(value);
    }
    
    render() {
        const { value } = this.props;
        if (value) {
            // Assume everything is JSON, because it pretty much is.
            return <Highlight className="json">
                    {Example.formatJson(value)}
                </Highlight>
        } else {
            return null;
        }
    }
}