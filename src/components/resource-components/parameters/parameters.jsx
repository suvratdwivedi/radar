'use strict'; 
import './parameters.less'

import React from 'react'
import Markdown from 'react-markdown'

import Description from '../description/description.jsx'
import { optionalFormatSpecifier } from '../format-specifier/format-specifier.jsx'

function capitalizeFirstLetter(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
}

function describeParamType(value) {
    const arrayItem = value.items;
    const itemType = arrayItem ? arrayItem.type : value.type;
    const format = (itemType === 'string') ? (arrayItem ? arrayItem.format : value.format) : null;

    const itemTypeDesc = (<span key="{arrayItem ? 'elementType' : 'type'}" className="parameter-type">
                <span className="type-value">{itemType}</span>
                {optionalFormatSpecifier(format)}
            </span>);
    return arrayItem ?
        (<span key="type" className="parameterType">array of {itemTypeDesc}</span>) :
        itemTypeDesc;
}

export default class Parameters extends React.Component {
    
    static existFor(resource) {
        return resource.parameters && resource.parameters.length > 0 &&
            (resource.parameters.length !== 1 || resource.parameters[0].in !== 'body');
    }
    
    static parameterTable(title, parameters, smallHeader) {
        return (<div key={title} className="resource-section">
                    {smallHeader ? <h4>{title}</h4> : <h3>{title}</h3>}
                    <table className="aui resource-parameters">           
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Description</th>
                            </tr>
                        </thead>         
                        <tbody>
                            {parameters.map(Parameters.parameter)}
                        </tbody>
                    </table>
                </div>);
    }
    
    static parameter(value) {
        let description = value.description ? <Markdown source={value.description} skipHtml={true}/> : null;
        if (value.enum || (value.items && value.items.enum)) {
            const enumValues = value.items ? value.items.enum : value.enum;
            const mdEnumList = enumValues.map(val => '- '+ val).join('\n\n');
            const enums = <Description key={value.name + '-enums'} title="Values" html={mdEnumList}/>;
            description = <td key="desc-col">{description}{enums}</td>;
        } else if (description) {
            description = <td key="desc-col">{description}</td>
        }
        
        if (description === null) {
            description = <td key="desc-col" className="no-description">(No Description)</td>;
        }
        
        let paramType = describeParamType(value);

        return (<tr key={value.name}>
                    <td key="name-col">
                        <span className="parameter">
                            <span key="name" className="parameter-name">
                                {value.name}
                                {value.required ? <span className="parameter-required"></span> : null}
                            </span>
                        </span>
                    </td>
                    <td key="type-col">
                        {paramType}
                    </td>
                    {description}
                </tr>);
    }
    
    static renderTypeTables(parameters, smallHeader) {
        let paramsByLocation = {};
        
        parameters.forEach(parameter => {
            if (paramsByLocation[parameter.in]) {
                paramsByLocation[parameter.in].push(parameter);
            } else {
                paramsByLocation[parameter.in] = [parameter];
            }
        });
        
        const parameterTables = [];
        for(let key in paramsByLocation) {
            parameterTables.push(Parameters.parameterTable(capitalizeFirstLetter(key) + ' parameters', paramsByLocation[key], smallHeader));
        }
        
        return (<div>
            {parameterTables}
        </div>);
    }
    
    render() {
        const { resource, smallHeader } = this.props;
        if (Parameters.existFor(resource)) {
            return Parameters.renderTypeTables(resource.parameters, smallHeader);
        } else {
            return null;
        }
    }
}