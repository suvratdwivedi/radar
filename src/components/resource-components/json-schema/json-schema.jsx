'use strict';
import './json-schema.less'

import _ from 'lodash'
import React from 'react'

import ExternalDocs from '../../external-docs/external-docs.jsx'
import { optionalFormatSpecifier } from '../format-specifier/format-specifier.jsx'
import Node from './components/node.jsx'
import NodeDescription from './components/node-description.jsx'
import NodeTitle from './components/node-title.jsx'
import NodeRange from './components/node-range.jsx'
import NodeRequired from './components/node-required.jsx'

const MAX_CYCLIC_NESTING = 2;

function mapItems(hideReadOnly) {
    return val => 
        val.type ? TYPE[val.type](val) :
             Object.keys(val).map(key => OF[key] ? OF[key](val, hideReadOnly) :
                 TYPE[key] ? TYPE[key](val, hideReadOnly) :
                     null);
}

function renderAllOf(value, hideReadOnly) {
    if (!value.allOf)
        return null;
    const items = value.allOf.map(mapItems(hideReadOnly));    
    return (<Node className="all-of-node">
        <NodeTitle title="all of"/>
        {items}
    </Node>);
}

function renderAnyOf(value, hideReadOnly) {
    if (!value.anyOf)
        return null;
    const items = value.anyOf.map(mapItems(hideReadOnly));
    return (<Node className="any-of-node">
        <NodeTitle title="any of"/>
        {items}
    </Node>);
}

function renderOneOf(value, hideReadOnly) {
    if (!value.oneOf)
        return null;
    const items = value.oneOf.map(mapItems(hideReadOnly));
    return (<Node className="one-of-node">
        <NodeTitle title="one of"/>
        {items}
    </Node>);
}

function renderArray(value, hideReadOnly, title, required) {
    return (<Node className="array-node">
        <NodeTitle title={title || value.title}/>
        <span className="node-tag"></span>        
        <NodeRange min={value.minItems || 0} max={value.maxItems || '∞'}/>
        <NodeRequired required={required}/>
        <NodeDescription value={value.description}/>
        {TYPE[value.items.type](value.items, hideReadOnly)}
        {renderAnyOf(value.items)}
        {renderAllOf(value.items)}
        {renderOneOf(value.items)}
    </Node>);
}

function renderBoolean(value, hideReadOnly, title, required) {
    return (<Node className="boolean-node">
        <NodeTitle title={title || value.title}/>
        <span className="node-type">{value.type}</span>
        <NodeRequired required={required}/>
        <NodeDescription value={value.description}/>        
    </Node>);
}

function renderInteger(value, hideReadOnly, title, required) {
    return (<Node className="integer-node">
        <NodeTitle title={title || value.title}/>
        <span className="node-type">{value.type}</span>
        <NodeRequired required={required}/>
        <NodeRange min={value.minimum} max={value.maximum}/>
        {value.enum ? renderEnum(value.enum) : null}        
        <NodeDescription value={value.description}/>        
    </Node>);
}

function renderNumber(value, hideReadOnly, title, required) {
    return (<Node className="number-node">
        <NodeTitle title={title || value.title}/>
        <span className="node-type">{value.type}</span>
        <NodeRequired required={required}/>
        {value.enum ? renderEnum(value.enum) : null}        
        <NodeDescription value={value.description}/>        
    </Node>);
}

function renderNull(value, hideReadOnly, title, required) {
    return (<Node className="null-node">
        <NodeTitle title={title || value.title}/>
        <span className="node-type">{value.type}</span>
        <NodeRequired required={required}/>
        <NodeDescription value={value.description}/>        
    </Node>);
}

function renderObject(value, hideReadOnly, title, required) {   
    let isShallow = false; 
    const properties = [];
    if (value.properties) {
        Object.keys(value.properties).forEach(key => {
            let val = value.properties[key];
            if (seenObjects.indexOf(val) !== -1) {
                ++currentNesting;
                isShallow = currentNesting >= MAX_CYCLIC_NESTING;
            } else if (val.type === 'object' || val.type === 'array') {
                seenObjects.push(val);
            }
        });
        if (!isShallow) {
            Object.keys(value.properties).forEach(key => {
                const prop = value.properties[key];
                const propRequired = value.required && (value.required.indexOf(key) !== -1);
                if (prop && (!hideReadOnly || !prop.readOnly)) {
                    properties.push(findRenderFunction(prop)(prop, hideReadOnly, key, propRequired));
                }
            });
        }
    }
    
    return (<Node className="object-node">
        <NodeTitle title={title || value.title}/>
        <span className="node-tag"></span>
        <NodeRequired required={required}/>
        <NodeDescription value={value.description}/>
        {isShallow ? <span>...</span> : properties}
    </Node>);
}

function renderString(value, hideReadOnly, title, required) {
    return (<Node className="string-node">
        <NodeTitle title={title || value.title}/>
        <span className="node-type">
            <span className="type-value">{value.type}</span>
            {optionalFormatSpecifier(value.format)}
        </span>
        <NodeRequired required={required}/>
        {value.enum ? renderEnum(value.enum) : null}
        <NodeDescription value={value.description}/>        
    </Node>);
}

function renderEnum(value) {
    return <Node className="enum-node">
        {(value || []).map(val => <span key={val} className="node-enum">{val}</span>)}  
    </Node>
}

const TYPE = {
    'array': renderArray,
    'boolean': renderBoolean,
    'integer': renderInteger,
    'number': renderNumber,
    'null': renderNull,
    'object': renderObject,
    'string': renderString,
    'undefined': _.noop
};

const OF = {
    'allOf': renderAllOf,
    'anyOf': renderAnyOf,
    'oneOf': renderOneOf,
    'undefined': _.noop
};

function findRenderFunction(value) {
    let fn = null;
    Object.keys(TYPE).forEach(key => value.type === key ? fn = TYPE[key] : null);
    Object.keys(OF).forEach(key => value[key] ? fn = OF[key] : null);
    return fn;    
}

let currentNesting;
let seenObjects = [];
export default class JsonSchema extends React.Component {
	render() {
        const { data, hideReadOnly } = this.props;
        const renderFn = findRenderFunction(data);
        currentNesting = 0;
        seenObjects = [];
        if (!renderFn)
            return null;
        return <div className="json-schema">{renderFn(data, hideReadOnly)}
            {ExternalDocs.fromOptionalObject(data.externalDocs)}</div>;        
	}
}