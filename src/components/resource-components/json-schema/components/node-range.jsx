'use strict';

import React from 'react'

export default class NodeRange extends React.Component {
    
    render() {
        const { min, max } = this.props;
        if (!min && !max) {
            return null;
        }
        return <span className="node-range">{(min || 0) + '..' + (max || '∞')}</span>
    }
} 