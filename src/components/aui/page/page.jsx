'use strict';
import './page.less'

import React from 'react'

export default class Page extends React.Component {
    
    render() {
        const { nav } = this.props;
        
        let navigation = null;
        if (nav) {
            navigation = (<div className="aui-page-panel-nav">
                            {nav}
                        </div>);
        }

        return (<div className="aui-page-panel">
                    <div className="aui-page-panel-inner">
                        {navigation}
                        <section className="aui-page-panel-content">
                            {this.props.children}
                        </section>
                    </div>
                </div>);
    }
}

