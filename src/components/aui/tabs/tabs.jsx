'use strict';
import './tabs.less'

import React from 'react'
import { Link } from 'react-router'

/**
 * Use AUI Tabs without the need for AUI JS. Yay.
 * 
 * The component expects an array of objects containing:
 * - content: JSX or string of the tab's content
 * - id: The tab's ID
 * - label: JSX or string of the tab's label
 */ 
export default class Tabs extends React.Component {
    
    static options() {
        return {
            layout: {
                HORIZONTAL: 'aui-tabs horizontal-tabs',
                VERTICAL: 'aui-tabs vertical-tabs'
            }
        }
    }
    
    static menuItem(forId, title, active, noHash, onClick) {
        let menuItemClass = 'menu-item';
        const idSelector = '#' + forId;
        if (active) {
            menuItemClass += ' active-tab';
        }
        
        return <li key={forId + '-' + title + 'menu-item'} className={menuItemClass} onClick={onClick ? () => onClick(forId) : null}>
                    {noHash ? <a>{title}</a> : <a href={idSelector}>{title}</a>}
                </li>
    }
    
    static tabsPane(id, content, active) {
        let tabsPaneClass = 'tabs-pane';
        if (active) {
            tabsPaneClass += ' active-pane';
        }
        return <div key={id + '-tabs-pane'} className={tabsPaneClass} id={id}>
                {content}
            </div>
    }

    render() {
        const { layout, values, selectedTab, noHash, onClick } = this.props;
        let actualSelectedTab = values[0].id;
        values.forEach((value) => {
            if (value.id === selectedTab) {
                actualSelectedTab = selectedTab
            }
        });
        return <div className={layout}>
                    <ul className="tabs-menu">
                        {values.map(value => Tabs.menuItem(value.id, value.label, actualSelectedTab === value.id, noHash, onClick))}
                    </ul>
                    {values.map(value => Tabs.tabsPane(value.id, value.content, actualSelectedTab === value.id))}
                </div>
    }    
}