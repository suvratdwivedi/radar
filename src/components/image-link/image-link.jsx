'use strict';
import './image-link.less'

import React from 'react'
import { Link } from 'react-router'

export default class ImageLink extends React.Component {
    
    render() {
        const { title, description, image, to } = this.props;
        return <div className="image-link">
                <Link to={to}>
                    <img src={image}/>
                    <h3>{title}</h3>
                    <p>{description}</p>
                </Link>
            </div>;
    }
}
