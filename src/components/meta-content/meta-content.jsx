'use strict';
import './meta-content.less'

import React from 'react'
import Markdown from 'react-markdown'

export default class MetaContent extends React.Component {
    
    render() {
        const page = this.props.page;

        return <div>
            <Markdown source={page.content}/>
        </div>;
    }
}
