'use strict';
import React from 'react'

const locations = {
    HEADER: 'header',
    QUERY: 'query'
};

export default class ApiKey extends React.Component {
    
    static toLocation(loc) {
        switch(loc) {
            case locations.HEADER:
                return 'Header';
            case locations.QUERY:
                return 'query parameter';
            default:
                return loc;
        }
    }
    
    render() {
        const { loc, name } = this.props;
        return <span>This should be supplied as a <strong>{ApiKey.toLocation(loc)}</strong> named <strong>{name}</strong>.</span>;
    }
}
