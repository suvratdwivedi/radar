'use strict';
import React from 'react'

const oAuthFlows = {
    'imlicit': 'implicit',
    'password': 'password',
    'application': 'application',
    'accessCode': 'access code'    
};

export default class OAuth extends React.Component {
    
    render() {
        const { flow, scopes, authorizationUrl, tokenUrl } = this.props;
        const scopesArr = [];
            for (let key in scopes) {
                scopesArr.push(<span><code>{key}</code> - {scopes[key]}</span>);
            }
        return <div>
                    <span>This API uses the <strong>{oAuthFlows[flow] || flow}</strong> flow.</span>
                    <p>Authorization URL: <a href={authorizationUrl}>{authorizationUrl}</a></p>
                    <p>Token URL: <a href={tokenUrl}>{tokenUrl}</a></p>
                    <h5>Scopes</h5>
                    <ul>
                        {scopesArr.map((scope, i) => <li key={i}>{scope}</li>)}
                    </ul>
                </div>;    ;
    }
}
