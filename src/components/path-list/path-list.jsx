'use strict';
import './path-list.less'

import _ from 'lodash'
import React from 'react'
import { Link } from 'react-router'

import { getLozengeClass, getSupportedMethods } from '../../utils/method-util'
import {qualifyResourcePath } from '../../utils/route-util'

export default class PathList extends React.Component {

	linkedLozenge(method, path) {
		return <span key={'lozenge-' + method + '-' + path} className={"aui-lozenge aui-lozenge-subtle " + getLozengeClass(method)}>
                <Link to={{
                    pathname: qualifyResourcePath(path),
                    hash: '#' + method
                }}>{method}</Link>
            </span>;
	}

	renderPaths() {
        const { resources } = this.props;
		let getMethodLozenges = (resource, path) => getSupportedMethods(resource).map(method => this.linkedLozenge(method, path));

		const sortedKeys = Object.keys(resources).sort();
        let sortedResources = {};
        sortedKeys.forEach(key => {
           sortedResources[key] = resources[key];
        });

		return _.map(sortedResources, (resource, path) => <tr key={path}>
				<td key={path + "-path"} className="path-column-data"><Link to={qualifyResourcePath(path)}>{path}</Link></td>
				<td key={path + "-methods"}>{getMethodLozenges(resource, path)}</td>
			</tr>);
	}

	render() {
		return <table className='aui resource-list-table'>
				<thead>
					<tr>
						<th>Path</th>
						<th>Methods</th>
					</tr>
				</thead>
				<tbody>
					{this.renderPaths()}
				</tbody>
			</table>
	}
}
