'use strict';
var del = require('del'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    Webpack = require('webpack'),
    WebpackDevServer = require('webpack-dev-server'),
    devConfig = require('./webpack/webpack.dev.config'),
    prodConfig = require('./webpack/webpack.prod.config');

gulp.task('develop', ['webpack:server']);
gulp.task('build:prod', ['webpack:prod']);

gulp.task('webpack:server', function (callback) {
    var compiler = Webpack(devConfig, webpackError(callback));
    
    webpackProgress(compiler);
    
    var server = new WebpackDevServer(compiler, {
        publicPath: '/js/',
        hot: true,
        quiet: false,
        noInfo: true,
        stats: {
            colors: true
        }
    });

    server.listen(9090, 'localhost', function () {
        console.log('Bundling project, please wait...');
    });
});

gulp.task('webpack:prod', function(callback) {
    Webpack(prodConfig, webpackError(callback));
});

function webpackError(callback) {
    return function(err, stats) {
        if (err) {
            throw new gutil.PluginError('webpack', err);
        }
        gutil.log('[webpack]', stats.toString());
        callback();
    }
}

function webpackProgress(compiler) {
    var bundleStart;
    compiler.plugin('compile', function() {
        console.log('Bundling...');
        bundleStart = Date.now();
    });
    
    compiler.plugin('done', function() {
        console.log('Bundled in ' + (Date.now() - bundleStart) + 'ms!');
    });
}