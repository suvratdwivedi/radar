jest.disableAutomock()
jest.mock('../../src/components/search/search.jsx')

import React from 'react'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'
import { mockStore } from '../../test-util'

import App from '../../src/containers/app.jsx'
import RestResource from '../../src/containers/rest-resource.jsx'

describe('Path Object', () => {
    const apiPath = {
        info: {
            title: 'Example API'
        },
        paths: {
            '/user': {
                'get': {},
                'put': {},
                'post': {},
                'delete': {},
                'options': {},
                'head': {},
                'patch': {},
                'parameters': [
                    {
                        'name': 'headerParam',
                        'in': 'header',
                        'type': 'string'
                    }
                ]
            }
        }
    };
        
    const store = mockStore(apiPath);
    const appRestResource = TestUtils.renderIntoDocument(<Provider store={store}>
            <App>
                <RestResource location={{pathname: "/resource/user"}}/>
            </App>
        </Provider>);
    const appNode = ReactDOM.findDOMNode(appRestResource);
    
    describe('resource', () => {
        const methods = appNode.querySelectorAll('.resource .aui-tabs ul li');
        
        it('renders all supported methods', () => {
            expect(methods.length).toEqual(7);
            const methodsArr = [...methods];
            const methodNames = methodsArr.map(val => val.textContent);
            expect(methodNames).toEqual(['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS']);
        });       
        
        it('renders path parameters', () => {
            const params = appNode.querySelectorAll('.resource .resource-parameters tbody tr');
            expect(params.length).toEqual(1);
            const paramArr = [...params];
           
            const paramCols = [...paramArr[0].querySelectorAll('td')];
            expect(paramCols.length).toEqual(3);
            expect(paramCols[0].textContent, 'param name').toEqual('headerParam');
            expect(paramCols[1].textContent, 'param type').toEqual('string');      
        });        
    })
});